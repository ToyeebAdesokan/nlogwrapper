﻿using System;

namespace NLogWrapper
{
    /// <summary>
    /// A message sent to the log
    /// </summary>
    public class LogMessage
    {
        /// <summary>
        /// The serial number of this log message
        /// </summary>
        public long SerialNo { get; set; }

        /// <summary>
        /// The date when this log was recorded
        /// </summary>
        public string Date { get; set; } = DateTime.Now.ToShortDateString();

        /// <summary>
        /// The time of the dat when this log was recorded
        /// </summary>
        public string Time { get; set; } = DateTime.Now.ToShortTimeString();

        /// <summary>
        /// The path where this message was logged i.e. The method that logged this message
        /// </summary>
        public string CallPath { get; set; }

        /// <summary>
        /// The level of log
        /// </summary>
        public string LogLevel { get; set; } = LogType.Info.ToString();

        /// <summary>
        /// Time elapsed since the last log message
        /// </summary>
        public string TimeElapsed { get; set; } = "0ms";

        /// <summary>
        /// The log message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The stack trace of the exception logged
        /// </summary>
        public string StackTrace { get; set; }
    }
}
