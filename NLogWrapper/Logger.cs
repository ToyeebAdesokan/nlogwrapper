﻿using System;
using Newtonsoft.Json;
using NLog;

namespace NLogWrapper
{
    /// <summary>
    /// Provides logging interface and utitlity functions.
    /// </summary>
    public partial class Logger
    {
        private NLog.Logger _logger;
        private static Logger _instance;
        private long _serialNumber = 1;
        private int _markerLength = 80;
        private char _marker = '*';

        private DateTime _lastLogTime = DateTime.MinValue;

        public Logger()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Singleton provider for the Logger interface.
        /// </summary>
        /// <returns>A Logger.</returns>
        public static Logger Get_()
        {
            if (_instance == null)
            {
                _instance = new Logger();
            }
            return _instance;
        }

        /// <summary>
        /// Processes a message into a standard log message.
        /// </summary>
        /// <param name="message">The message to process</param>
        /// <param name="logType">The log type.</param>
        /// <returns>The processed message.</returns>
        protected string ProcessedMessage(LogMessage message)
        {
            DateTime now = DateTime.Now;
            TimeSpan timeElapsed = _lastLogTime == DateTime.MinValue ? TimeSpan.Zero : now.Subtract(_lastLogTime);

            // Update the last time we touched the log.
            _lastLogTime = now;

            message.CallPath = LogManager.GetDebugInfo();
            message.SerialNo = _serialNumber++;
            message.TimeElapsed = $"{timeElapsed.TotalMilliseconds.ToString()}ms";

            return JsonConvert.SerializeObject(message, Formatting.Indented);
        }

        /// <summary>
        /// Writes the diagonistic message at Info level.
        /// </summary>
        /// <param name="message">Log message</param>
        public void Info(string message)
        {
            _logger?.Info(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Info.ToString(),
            }));
        }

        /// <summary>
        /// Writes the diagonistic message and exception at Info level.
        /// </summary>
        /// <param name="message">A string to be written</param>
        /// <param name="exception">An exception to be logged</param>
        [Obsolete]
        public void InfoException(string message, Exception exception)
        {
            _logger?.InfoException(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Info.ToString(),
                StackTrace = exception.StackTrace
            }), exception);
        }

        /// <summary>
        /// Writes the diagonistic message at Error level.
        /// </summary>
        /// <param name="message">Log message</param>
        public void Error(string message)
        {
            _logger?.Error(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Error.ToString(),
            }));
        }

        /// <summary>
        /// Writes the diagonistic message and exception at Error level.
        /// </summary>
        /// <param name="message">A string to be written</param>
        /// <param name="exception">An exception to be logged</param>
        [Obsolete]
        public void ErrorException(string message, Exception exception)
        {
            _logger?.ErrorException(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Error.ToString(),
                StackTrace = exception.StackTrace
            }), exception);
        }

        /// <summary>
        /// Writes the diagonistic message at Debug level.
        /// </summary>
        /// <param name="message">Log message</param>
        public void Debug(string message)
        {
            _logger?.Debug(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Debug.ToString(),
            }));
        }

        /// <summary>
        /// Writes the diagonistic message and exception at Debug level.
        /// </summary>
        /// <param name="message">A string to be written</param>
        /// <param name="exception">An exception to be logged</param>
        [Obsolete]
        public void DebugException(string message, Exception exception)
        {
            _logger?.DebugException(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Debug.ToString(),
                StackTrace = exception.StackTrace
            }), exception);
        }

        /// <summary>
        /// Writes the diagonistic message at Debug level.
        /// </summary>
        /// <param name="message">Log message</param>
        public void Trace(string message)
        {
            _logger?.Trace(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Trace.ToString()
            }));
        }

        /// <summary>
        /// Writes the diagonistic message and exception at Debug level.
        /// </summary>
        /// <param name="message">A string to be written</param>
        /// <param name="exception">An exception to be logged</param>
        [Obsolete]
        public void TraceException(string message, Exception exception)
        {
            _logger?.TraceException(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Trace.ToString(),
                StackTrace = exception.StackTrace
            }), exception);
        }

        /// <summary>
        /// Writes the diagonistic message at Log level.
        /// </summary>
        /// <param name="logLevel">The log level</param>
        /// <param name="message">Log message</param>
        public void Log(LogLevel logLevel, string message)
        {
            _logger?.Log(logLevel, ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Info.ToString()
            }));
        }

        /// <summary>
        /// Writes the diagonistic message and exception at Log level.
        /// </summary>
        /// <param name="message">The log leveln</param>
        /// <param name="message">A string to be written</param>
        /// <param name="exception">An exception to be logged</param>
        [Obsolete]
        public void LogException(LogLevel logLevel, string message, Exception exception)
        {
            _logger?.LogException(logLevel, ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Info.ToString(),
                StackTrace = exception.StackTrace
            }), exception);
        }

        /// <summary>
        /// Writes the diagonistic message at Warning level.
        /// </summary>
        /// <param name="message">Log message</param>
        public void Warn(string message)
        {
            _logger?.Warn(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Warning.ToString()
            }));
        }

        /// <summary>
        /// Writes the diagonistic message and exception at Warning level.
        /// </summary>
        /// <param name="message">A string to be written</param>
        /// <param name="exception">An exception to be logged</param>
        [Obsolete]
        public void WarnException(string message, Exception exception)
        {
            _logger?.WarnException(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Warning.ToString(),
                StackTrace = exception.StackTrace
            }), exception);
        }

        /// <summary>
        /// Writes the diagonistic message at Warning level.
        /// </summary>
        /// <param name="message">Log message</param>
        public void Fatal(string message)
        {
            _logger?.Fatal(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Fatal.ToString()
            }));
        }

        /// <summary>
        /// Writes the diagonistic message and exception at Warning level.
        /// </summary>
        /// <param name="message">A string to be written</param>
        /// <param name="exception">An exception to be logged</param>
        [Obsolete]
        public void FatalException(string message, Exception exception)
        {
            _logger?.FatalException(ProcessedMessage(new LogMessage
            {
                Message = message,
                LogLevel = LogType.Fatal.ToString(),
                StackTrace = exception.StackTrace
            }), exception);
        }

        /// <summary>
        /// Starts a log region
        /// </summary>
        /// <param name="title">The title of the region</param>
        public void StartRegion(string title = null)
        {
            if (title == null)
            {
                title = "Start";
            } else
            {
                title = $"Start {title}";
            }

            int markerCount = (_markerLength - title.Length - 2) / 2;
            string message = null;

            if (markerCount >= 1)
            {
                string marker = new string(_marker, markerCount);
                message = $"{marker} {title.ToUpper()} {marker}";
            }

            _logger.Log(_logger.IsTraceEnabled ? LogLevel.Trace : LogLevel.Info, $"{message}");
        }

        /// <summary>
        /// Ends a log region
        /// </summary>
        /// <param name="title">The title of the region</param>
        public void EndRegion(string title = null)
        {
            if (title == null)
            {
                title = "End";
            } else
            {
                title = $"End {title}";
            }

            int markerCount = (_markerLength - title.Length - 2) / 2;
            string message = null;

            if (markerCount >= 1)
            {
                string marker = new string(_marker, markerCount);
                message = $"{marker} {title.ToUpper()} {marker}";
            }

            _logger.Log(_logger.IsTraceEnabled ? LogLevel.Trace : LogLevel.Info, $"{message}\n\n");
        }
    }
}
