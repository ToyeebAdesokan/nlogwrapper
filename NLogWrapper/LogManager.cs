﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace NLogWrapper
{
    /// <summary>
    /// Creates and manages instances of ZoneSwitch.NLogWrapper.Logger objects.
    /// </summary>
    public class LogManager
    {
        /// <summary>
        /// Gets the Logger with the name of the current class
        /// </summary>
        /// <returns>A valid Logger</returns>
        public static Logger GetCurrentClassLogger()
        {
            return Logger.Get_();
        }

        /// <summary>
        /// Returns information about who is calling the current method
        /// </summary>
        /// <returns>Debug info on the method calling the log</returns>
        public static string GetDebugInfo()
        {
            StackFrame frame = new StackTrace().GetFrame(3);
            MethodBase method = frame.GetMethod();

            return $"{method.ReflectedType.FullName}@{method}";
        }
    }
}
