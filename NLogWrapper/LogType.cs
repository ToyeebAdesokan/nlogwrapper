﻿namespace NLogWrapper
{
    /// <summary>
    /// Types of logs in the system
    /// </summary>
    public enum LogType
    {
        /// <summary>
        /// An error log that throws no exception.<br/>
        /// E.g. "Hyperledger timed out."
        /// </summary>
        Error,

        /// <summary>
        /// A general log for debugging purposes only.<br/>
        /// E.g. "Transaction requests sent in GetCustomerDetails()."
        /// </summary>
        Debug,

        /// <summary>
        /// A log type that only provides information<br/>
        /// E.g. "Response was received successfully."
        /// </summary>
        Info,

        /// <summary>
        /// A log type to warn of possible faliure of data loss in the system but 
        /// does not halt operation.<br/>
        /// E.g. "The transaction reference returned was empty."
        /// </summary>
        Warning,

        /// <summary>
        /// A log that is only used to trace an exception that occured but is not thrown.<br/>
        /// E.g. "Conversion failed. Trxref isn't a string. See -> ..."
        /// </summary>
        Trace,

        /// <summary>
        /// An error log that throws exception and requires great attention.<br/>
        /// E.g. "Could not reach Hyperledger on port 8080. Hyperledger could be down."
        /// </summary>
        Fatal,
    }
}
